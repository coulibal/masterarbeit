FROM openjdk:8
LABEL maintainer="Franck Coulibaly (coulibal@th-brandenburg.de)"
RUN echo "Europe/Berlin" > /etc/timezone

ADD ./target/masterarbeit-0.0.1-SNAPSHOT.jar /masterarbeit-0.0.1-SNAPSHOT.jar
EXPOSE 9091
ENTRYPOINT ["java","-jar","/masterarbeit-0.0.1-SNAPSHOT.jar"]


#ENTRYPOINT ["java","-Dserver.port=$PORT $JAVA_OPTS -jar"," /masterarbeit-0.0.1-SNAPSHOT.jar"]

