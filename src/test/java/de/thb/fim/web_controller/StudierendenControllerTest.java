package de.thb.fim.web_controller;

import de.thb.fim.model.StudierendenEntity;
import de.thb.fim.service.StudierendenService;

import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class) // sonst error Null Pointer exception
@WebMvcTest(StudierendenController.class)

public class StudierendenControllerTest {

	// class under Test
	@MockBean
	private StudierendenService studierendenService;

	@Autowired
	private MockMvc mvc;

	ObjectMapper mapper = new ObjectMapper();
	String resultContent = null;

//    
	@Test
	public void getAllStudierendenTest() throws Exception {

		List<StudierendenEntity> returnList = Arrays.asList(
				new StudierendenEntity("Muster", "Mann", "Muster@mgail.com"),
				new StudierendenEntity("Boat", "Test", "Boat@yahoo.com"));

		resultContent = mapper.writeValueAsString(returnList);
		when(studierendenService.getAllStudierenden()).thenReturn(returnList);

		mvc.perform(get("/").contentType("application/json")).andExpect(status().isOk())

				.andDo(print());
	}

	@Test
	public void createStudierendeTest() throws Exception {
		final long mnr = 1L;

		StudierendenEntity en = new StudierendenEntity("Muster", "Frau", "Muster@gmx.des");
		en.setMnr(mnr);
		String studierende = mapper.writeValueAsString(en);
		when(studierendenService.create_updateStudierende(en)).thenReturn(en);

		mvc.perform(post("/createStudierende").content(studierende)

				.contentType("application/json")).andExpect(status().is3xxRedirection

		()).andDo(print());

	}

	@Test
	public void createStudierendeAPITest() throws Exception {
		final long mnr = 1L;

		StudierendenEntity en = new StudierendenEntity("Teri", "Dactyl", "Dactyl@test.com");
		en.setMnr(mnr);
		String studierende = mapper.writeValueAsString(en);
		when(studierendenService.create_updateStudierende(en)).thenReturn(en);

		mvc.perform(post("/create").content(studierende)

				.contentType("application/json")).andExpect(status().is3xxRedirection

		()).andDo(print());

	}

	@Test
	public void updateStudierendeByMnrTest() throws Exception {
		final long mnr = 2L;

		StudierendenEntity en = new StudierendenEntity("Paddy", "Furniture", "Paddy@yahoo.com");
		en.setMnr(mnr);
		String studierende = mapper.writeValueAsString(en);
		when(studierendenService.getStudierendeByMnr(en.getMnr())).thenReturn(en);

		this.mvc.perform(put("/update/2").content(studierende).contentType("application/json"))
				.andExpect(status().isOk());
		// .andExpect(status().is3xxRedirection());
	}

	@Test
	public void updateStudierendeByMnrAPITest() throws Exception {
		final long mnr = 2L;

		StudierendenEntity en = new StudierendenEntity("Grater", "Allie", "Allie@gmx.de");
		en.setMnr(mnr);
		String studierende = mapper.writeValueAsString(en);
		when(studierendenService.getStudierendeByMnr(en.getMnr())).thenReturn(en);

		this.mvc.perform(put("/edit/2").content(studierende).contentType("application/json"))
				// .andExpect(status().isOk())
				.andExpect(status().is3xxRedirection());
	}

	@Test
	public void deleteStudierendeByMnrTest() throws Exception {
		final long mnr = 1L;

		StudierendenEntity en = new StudierendenEntity("Maureen", "Biologist", "Maureen@boat.com");
		en.setMnr(mnr);
		String studierende = mapper.writeValueAsString(en);
		when(studierendenService.getStudierendeByMnr(en.getMnr())).thenReturn(en);

		this.mvc.perform(delete("/delete/2").content(studierende)

				.contentType("application/json")).andExpect(status().is3xxRedirection

		());
		
		
		
	}
}
