package de.thb.fim.service;

import de.thb.fim.exception.StudierendenNotFoundException;
import de.thb.fim.model.StudierendenEntity;
import de.thb.fim.repository.StudierendenRepository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class StudierendenServiceTest {

	// //(all methode in der repository funktionieren)
	@MockBean
	private StudierendenRepository studierendenRepository;
	@Autowired
	private StudierendenService underTest; //Class under test

	private StudierendenEntity studierendenEntity;

	@TestConfiguration
	static class StudierendenServiceConfig {
		@Bean
		public StudierendenService stService() {
			return new StudierendenService();
		}
	}

	@Test
	@DisplayName("")
	public void create_updateStudierendeTest() {
		
		studierendenEntity = new StudierendenEntity();
		studierendenEntity.setMnr(20281798L);
		studierendenEntity.setVorname("Olive");
		studierendenEntity.setNachname("Tree");
		studierendenEntity.setMail_adresse("Olive@gmail.com");

		underTest.create_updateStudierende(studierendenEntity);
		verify(studierendenRepository).save(studierendenEntity);

	}

	@Test
	@DisplayName("")
	public void getStudierendeByMnrTest() throws StudierendenNotFoundException {
		studierendenEntity = new StudierendenEntity();

		studierendenEntity.setMnr(20221798L);
		studierendenEntity.setVorname("Muster");
		studierendenEntity.setNachname("Mann");
		studierendenEntity.setMail_adresse("muster@test@gmail.com");

		when(studierendenRepository.findById(studierendenEntity.getMnr())).thenReturn(Optional.of(studierendenEntity));

		StudierendenEntity studierende = underTest.getStudierendeByMnr(studierendenEntity.getMnr());

		verify(studierendenRepository).findById(studierende.getMnr());

		assertEquals(studierendenEntity.getMnr(), studierende.getMnr());
		assertEquals(studierendenEntity.getVorname(), studierende.getVorname());
		assertEquals(studierendenEntity.getNachname(), studierende.getNachname());
		assertEquals(studierendenEntity.getMail_adresse(), studierende.getMail_adresse());

	}
	
	


	@Test
	@DisplayName("should_throw_exception_when_studierende_doesnt_exist")
	public void getStudierendeByMnrTest_Exception_Test()  {
		studierendenEntity = new StudierendenEntity();

		studierendenEntity.setMnr(20221798L);
		studierendenEntity.setVorname("Muster");
		studierendenEntity.setNachname("Mann");
		studierendenEntity.setMail_adresse("muster@test@gmail.com");

		when(studierendenRepository.findById(studierendenEntity.getMnr())).thenReturn(Optional.ofNullable(null));


		
	

		assertThrows(StudierendenNotFoundException.class, () -> {
			underTest.getStudierendeByMnr(studierendenEntity.getMnr());
	    });

		
	}

	@Test
	public void getAllStudierendenTest()

	{

		List<StudierendenEntity> returnList = Arrays.asList(
				new StudierendenEntity("Muster", "Mann", "coulibal@th-brandenburg.de"),
				new StudierendenEntity("Top", "Mann", "coulibal@th-brandenburg.de"));
		when(underTest.getAllStudierenden()).thenReturn(returnList);
		List<StudierendenEntity> persons = underTest.getAllStudierenden();

		StudierendenEntity st1 = returnList.get(0);
		StudierendenEntity st2 = returnList.get(1);
		assertEquals(2, persons.size());
		assertThat(persons.contains(st1));
		assertThat(persons.contains(st2));
	}

	@Test
	public void deleteStudierendeByMnrTest() throws StudierendenNotFoundException {
		// Arrange
		studierendenEntity = new StudierendenEntity();

		studierendenEntity.setMnr(1L);
		studierendenEntity.setVorname("12");
		studierendenEntity.setMail_adresse("12");
		studierendenEntity.setNachname("12");
		underTest.create_updateStudierende(studierendenEntity);
		when(studierendenRepository.findById(studierendenEntity.getMnr())).thenReturn(Optional.of(studierendenEntity));

		// Act
		underTest.deleteStudierendeByMnr(1L);
		// Assert
		verify(studierendenRepository, times(1)).deleteById(studierendenEntity.getMnr());
	}

	@Test
	@DisplayName("should_throw_exception_when_studierende_doesnt_exist")
	public void deleteStudierendeByMnr_Exception_Test()  {
		studierendenEntity = new StudierendenEntity();

		studierendenEntity.setMnr(1L);
		studierendenEntity.setVorname("12");
		studierendenEntity.setMail_adresse("12");
		studierendenEntity.setNachname("12");

		when(studierendenRepository.findById(studierendenEntity.getMnr())).thenReturn(Optional.ofNullable(null));
	
		assertThrows(StudierendenNotFoundException.class, () -> {
			underTest.deleteStudierendeByMnr(studierendenEntity.getMnr());
	    });

	}
	
	
	

}
