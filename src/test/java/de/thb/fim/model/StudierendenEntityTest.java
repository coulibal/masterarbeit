package de.thb.fim.model;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StudierendenEntityTest {

    //Class under Test
    private StudierendenEntity underTest;
    @BeforeEach()
    public void setUp() throws Exception{
        underTest = new StudierendenEntity();
    }

    @AfterEach
    public void tearDown() throws Exception{
        underTest = null;
        assertNull(underTest);
    }

    @Test
    public void testGetMnr() {
        final long mnr = 1L;
        underTest.setMnr(mnr);

        final long result = underTest.getMnr();

        assertEquals(result, mnr);
    }

    @DisplayName("Runnning before each test")
    @Test
    public void testGetFirstName() {
        final String firstName = "FirstName";
        underTest.setVorname(firstName);

        final String result = underTest.getVorname();

        assertEquals(result, firstName);
    }

    @Test
    public void testGetFirstName_Null() {
        final String vorname = null;
        underTest.setVorname(vorname);

        final String result = underTest.getVorname();

        assertNull(vorname);
        assertEquals(result, vorname);
    }

    @Test
    public void testGetLastName() {
        final String lastName = "LastName";
        underTest.setNachname(lastName);

        final String result = underTest.getNachname();

        assertEquals(result, lastName);
    }

    @Test
    public void testGetLastName_Null() {
        final String lastName = null;
        underTest.setNachname(lastName);

        final String result = underTest.getNachname();

        assertNull(lastName);
        assertEquals(result, lastName);
    }
    @Test
    public void testGetMailAdresse() {
        final String mail_adresse = "email@test.com";
        underTest.setMail_adresse(mail_adresse);

        final String result = underTest.getMail_adresse();

        assertEquals(result, mail_adresse);
    }  

    @Test
    public void testGetMailAdresse_Null() {
        final String mail_adresse = null;
        underTest.setMail_adresse(mail_adresse);

        final String result = underTest.getMail_adresse();

        assertNull(mail_adresse);
        assertEquals(result, mail_adresse);
    }

    @Test
    public void testReset() {
        final String empty = "";
        final long zero = 0;

        underTest.reset();

        assertEquals(underTest.getVorname(), empty);
        assertEquals(underTest.getNachname(), empty);
        assertEquals(underTest.getMnr(), zero);
        assertEquals(underTest.getMail_adresse(), empty);

    }
 

}