package de.thb.fim.integrationstest_web_controller;

import de.thb.fim.Studierendenverwaltungssystems;
import de.thb.fim.model.StudierendenEntity;
import de.thb.fim.service.StudierendenService;

import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class) // sonst error Null Pointer exception
@Tag("Integrationstests")
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT, classes = { Studierendenverwaltungssystems.class })
public class StudierendenControllerIntegrationTest {

	// class under Test
	@Autowired
	private StudierendenService studierendenService;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	ObjectMapper mapper = new ObjectMapper();
	String resultContent = null;

	@BeforeEach
	public void setup() {

		mvc = MockMvcBuilders.webAppContextSetup(context).build();

	}

	@Test
	@Order(1)
	public void createStudierendeAPITest() throws Exception {
		final long mnr = 1L;

		StudierendenEntity stEntity = new StudierendenEntity("Test", "Boat", "boat@gmail.com");
		stEntity.setMnr(mnr);
		String studierende = mapper.writeValueAsString(stEntity);

		mvc.perform(post("/create").content(studierende)

				.contentType("application/json")).andExpect(status().is3xxRedirection

		()).andDo(print());

	}

	@Test
	@Order(2)
	public void updateStudierendeByMnrAPITest() throws Exception {
		StudierendenEntity stEntity = studierendenService.getStudierendeByMnr(1L);
		if (Objects.nonNull(stEntity)) {

			StudierendenEntity s = new StudierendenEntity();
			s.setNachname("Frau");
			s.setVorname("Muster");
			s.setMail_adresse("boat@test.com");

			String studierende = mapper.writeValueAsString(s);

			System.err.print(stEntity.getMnr());

			this.mvc.perform(put("/edit/" + stEntity.getMnr()).content(studierende).contentType("application/json"))

					.andExpect(status().is3xxRedirection());
		}
	}
	

//  
	@Test
	@Order(3)
	public void getAllStudierendenTest() throws Exception {

		mvc.perform(get("/").contentType("application/json")).andExpect(status().isOk())

				.andDo(print());
	}

	@Test
	@Order(4)
	public void deleteStudierendeByMnrTest() throws Exception {
		StudierendenEntity stEntity = studierendenService.getStudierendeByMnr(1L);

		if (Objects.nonNull(stEntity)) {

			this.mvc.perform(delete("/delete/" + stEntity.getMnr())

					.contentType("application/json")).andExpect(status().is3xxRedirection()).andDo(print());

			getAllStudierendenTest();
		}
	}
}