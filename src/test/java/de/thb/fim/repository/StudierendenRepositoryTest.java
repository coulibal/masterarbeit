package de.thb.fim.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

// um der JPA Repository zu testen
//Integration Test, um der API End-Points zu testen (CRUD)
import de.thb.fim.model.StudierendenEntity;

@ExtendWith(SpringExtension.class) // sonst error Null Pointer exception
@DataJpaTest
public class StudierendenRepositoryTest {

	@Autowired
	private StudierendenRepository underTest;

	@AfterEach
	public void tearDown() throws Exception {
		underTest.deleteAll();
	}

	@Test
	public void testSaveEmployee() {
		final long mnr = 1L;

		StudierendenEntity studierende = new StudierendenEntity("Muster", "Mann", "Muster@email.com");
		studierende.setMnr(mnr);
		underTest.save(studierende);
		StudierendenEntity studierende2 = underTest.findByNachname("Mann");
		assertEquals(studierende2.getVorname(), studierende.getVorname());
		assertEquals(studierende2.getNachname(), studierende.getNachname());

	}

	@Test
	public void testGetStudierende() {
		final long mnr = 1L;


		StudierendenEntity studierende = new StudierendenEntity("Muster", "Frau", "Frau@gmx.de");
		studierende.setMnr(mnr);

		underTest.save(studierende);

		StudierendenEntity studierende2 = underTest.findByNachname("Frau");
		assertNotNull(studierende);
		assertEquals(studierende2.getVorname(), studierende.getVorname());
		assertEquals(studierende2.getNachname(), studierende.getNachname());

	}

	@Test
	public void testDeleteStudierende() {
		final long mnr = 2L;

		StudierendenEntity studierende = new StudierendenEntity("Peg", "Leger", "Leger@gmx.de");
		studierende.setMnr(mnr);

		underTest.save(studierende);
		underTest.delete(studierende);
	}

	@Test
	public void findAllStudierenden() {
		final long mnr_1 = 1L;
		final long mnr_2 = 2L;


		StudierendenEntity studierende = new StudierendenEntity("Muster", "Mann", "Mann@gmx.de");
		StudierendenEntity studierende2 = new StudierendenEntity("Muster", "Frau", "Frau@gmx.de");
		studierende.setMnr(mnr_1);
		studierende2.setMnr(mnr_2);

		underTest.save(studierende);
		underTest.save(studierende2);

		//assertNotNull(underTest.findAll());
	assertEquals(2, underTest.count());
	}

	@Test
	public void deleteByMnrStudierendenTest() {
		final long mnr = 2L;

		StudierendenEntity studierende = new StudierendenEntity("Olive", "Yew", "Olive@test,com");
		studierende.setMnr(mnr);

		StudierendenEntity emp = underTest.save(studierende);
		underTest.deleteById(emp.getMnr());
	}

	@Test
	public void FindByStudierendenByMnrTest() {

		final long mnr = 1L;

		StudierendenEntity studierende = new StudierendenEntity( "Anne", "Thurium", "Thurium@yahoo.com");
		studierende.setMnr(mnr);

		underTest.save(studierende);
		final long result = studierende.getMnr();

		assertEquals(result, mnr);

	}

}
