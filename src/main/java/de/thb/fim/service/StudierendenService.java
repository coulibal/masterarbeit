package de.thb.fim.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.thb.fim.exception.StudierendenNotFoundException;
import de.thb.fim.model.StudierendenEntity;
import de.thb.fim.repository.StudierendenRepository;

@Service
public class StudierendenService {

	@Autowired
	StudierendenRepository repository;

	public StudierendenEntity create_updateStudierende(StudierendenEntity entity) {

		return repository.save(entity);

	}

	public void deleteStudierendeByMnr(long mnr) throws StudierendenNotFoundException {
		Optional<StudierendenEntity> studierende = repository.findById(mnr);

		if (studierende.isPresent()) {
			repository.deleteById(mnr);
		} else {
			throw new StudierendenNotFoundException("Es existiert keine Studierende mit dieser Matrikelnummer");
		}
	}

	public List<StudierendenEntity> getAllStudierenden() {
		List<StudierendenEntity> result = (List<StudierendenEntity>) repository.findAll();

		if (result.size() > 0) {
			return result;
		} else {
			return new ArrayList<StudierendenEntity>();
		}
	}

	public StudierendenEntity getStudierendeByMnr(long mnr) throws StudierendenNotFoundException  {
		Optional<StudierendenEntity> studierende = repository.findById(mnr);

		if (studierende.isPresent()) {
			return studierende.get();
		} else {
			throw new StudierendenNotFoundException("Es existiert keine Studierende mit dieser Matrikelnummer");
		}
	}

}
  