package de.thb.fim.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.thb.fim.model.StudierendenEntity;

@Repository
public interface StudierendenRepository 
			extends CrudRepository<StudierendenEntity, Long> {
	

	public StudierendenEntity findByNachname(String nachname); // search with Name


}
 