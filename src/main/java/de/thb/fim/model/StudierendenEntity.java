package de.thb.fim.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class StudierendenEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long mnr;

	@Column(name = "vorname", nullable = false, length = 100)
	private String vorname;

	@Column(name = "nachname", nullable = false, length = 100)
	private String nachname;

	@Column(name = "mail_adresse", nullable = false, length = 200)
	private String mail_adresse;

	/**
	 * @param vorname
	 * @param nachname
	 * @param mail_adresse
	 */

	public StudierendenEntity(String vorname, String nachname, String mail_adresse) {
		setVorname(vorname);
		setNachname(nachname);
		setMail_adresse(mail_adresse);

	}

	public StudierendenEntity()

	{
		this(null, null, null);
	}

	public long getMnr() {
		return mnr;
	}

	public void setMnr(long mnr) {
		this.mnr = mnr;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getMail_adresse() {
		return mail_adresse;
	}

	public void setMail_adresse(String mail_adresse) {
		this.mail_adresse = mail_adresse;
	}

	public void reset() {
		this.setMnr(0);
		this.setMail_adresse("");
		this.setNachname("");
		this.setVorname("");

	}

}
