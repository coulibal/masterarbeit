package de.thb.fim.web_controller;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.thb.fim.exception.StudierendenNotFoundException;
import de.thb.fim.model.StudierendenEntity;
import de.thb.fim.service.StudierendenService;

/*
 * 
 * Klasse für die Verarbeitung von API-CRUD-Anfragen
 */

@Controller
@RequestMapping("/")
public class StudierendenController {
	@Autowired // Inject Dependency
	StudierendenService service;
	

	@RequestMapping(path = "/createStudierende", method = RequestMethod.POST) // Url Mapping
	public String createStudierende(StudierendenEntity studierende) {
		service.create_updateStudierende(studierende);
		return "redirect:/";
	}

	@RequestMapping(value = { "/update", "/update/{mnr}" })
	public String updateStudierendeByMnr(Model model, @PathVariable("mnr") Optional<Long> mnr)
			throws StudierendenNotFoundException {
		if (mnr.isPresent()) {
			StudierendenEntity entity = service.getStudierendeByMnr(mnr.get());
			model.addAttribute("studierende", entity); //
		} else {
			model.addAttribute("studierende", new StudierendenEntity());
		}
		return "create-update-studierende";
	}

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public String createAPI(@RequestBody StudierendenEntity studierende) { 	// @RequestBody für API-Abfrage
		service.create_updateStudierende(studierende);
		return "redirect:/";
	}

	@RequestMapping(value = { "/edit/{mnr}" })
	public String updateAPI(@PathVariable(value = "mnr") long mnr,
			@RequestBody(required = false) StudierendenEntity studierende) throws StudierendenNotFoundException {
		StudierendenEntity sEntity = service.getStudierendeByMnr(mnr);
		if (Objects.isNull(sEntity)) {
			throw new StudierendenNotFoundException("Es existiert keine studierende mit dieser ID");

		}

		studierende.setMnr(mnr);

		service.create_updateStudierende(studierende);
		return "redirect:/";
	}

	@RequestMapping
	public String getAllStudierenden(Model model) { 
		List<StudierendenEntity> list = service.getAllStudierenden();

		model.addAttribute("studierenden", list); // Alle Studierenden im Modell speichern. (Key, value)
		return "get-studierende"; // Ergebnis zur Thymeleaf-Ansicht zurückgeben

	}

	@RequestMapping(path = "/delete/{mnr}")
	public String deleteStudierendeByMnr(Model model, @PathVariable("mnr") long mnr)
			throws StudierendenNotFoundException {
		service.deleteStudierendeByMnr(mnr);
		return "redirect:/";
	}
}
  


  