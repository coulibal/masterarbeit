package de.thb.fim;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Studierendenverwaltungssystems {

	public static void main(String[] args) {
		SpringApplication.run(Studierendenverwaltungssystems.class, args);
		
		
	}

	
}
